# GameDev roadmap
This is a list of courses, materials, books which may help you to get into game-dev industry. Some of them cost money, some are free. The list is based on my own experience and is used by me frequently. Also, lots of Job Titles across the internet were taken into consideration during the creation. You can check them separately in the attached Wiki.    
Have a nice learning and good luck!

---

## Alghoritms and Data Sructures

CS50   
http://cs50.tv/2017/fall/ 


CS50 (Prometheus)
https://courses.prometheus.org.ua/courses/Prometheus/CS50/2016_T1/about

Розробка та аналіз алгоритмів (UKR)   
https://edx.prometheus.org.ua/courses/KPI/Algorithms101/2015_Spring/about



Data Structures and Algorithms Specialization   
https://www.coursera.org/specializations/data-structures-algorithms

Fundamentals of Algorithms    
https://www.geeksforgeeks.org/fundamentals-of-algorithms


Algorithms Part 1 (Coursera)
https://www.coursera.org/learn/algorithms-part1

Algorithms Part 2 (Coursera)
https://www.coursera.org/learn/algorithms-part2

---
## C#

C# Tutorial (SoloLearn)   
https://www.sololearn.com/Course/CSharp/ 

C# learning path (Pluralsight)   
https://app.pluralsight.com/paths/skill/csharp

---
## Unitu (C#)

Unity Documentation   
https://docs.unity3d.com/Manual/index.html


Unity Game Dev Courses: Design   
https://app.pluralsight.com/paths/skill/unity-game-dev-courses-design


Unity Game Dev Courses: Fundamentals   
https://app.pluralsight.com/paths/skill/unity-game-development-core-skills


Unity Game Dev Courses: Art   
https://app.pluralsight.com/paths/skill/unity-game-dev-courses-art

Unity Game Dev Courses: Programming   
https://app.pluralsight.com/paths/skill/unity-game-dev-courses-programming

---
## C/C++ 
   
   
C Tutorial (SoloLearn)   
https://www.sololearn.com/Course/C/

C++ Tutorial (SoloLearn)    
https://www.sololearn.com/Course/CPlusPlus/

C full documentation with exmaples  
https://www.geeksforgeeks.org/c-programming-language/

C++ full documentation with examples  
https://www.geeksforgeeks.org/c-plus-plus/

C++ learning path (Pluralsight)   
https://app.pluralsight.com/paths/skill/c-plus-plus

Become a C++ Developer   
https://www.lynda.com/learning-paths/Developer/become-a-c-plus-plus-developer

Learn Advanced C++ Programming    
https://www.udemy.com/learn-advanced-c-programming/


- - -
## Unreal Engine (Blueprints/C++)

UE4 Documentation    
https://docs.unrealengine.com/en-us/

Unreal Engine 4 Mastery: Create Multiplayer Games with C++   
https://www.udemy.com/unrealengine-cpp/


Unreal CPP   
*Batch of practical tutorials*    
https://unrealcpp.com/


Unreal Engine Live Seminars (Twitch)    
https://www.twitch.tv/unrealengine/videos

---
## Programming practice

Code Clinic: C   
https://www.lynda.com/C-tutorials/Code-Clinic-C/713386-2.html

Code Clinic: C++   
https://www.lynda.com/C-tutorials/Code-Clinic-C/162139-2.html  

Code Clinic: C#  
https://www.lynda.com/C-tutorials/Code-Clinic-C/765321-2.html



Coding Game (Gameplay/AI programming, heavily relies on algorithms and data structures)     
*Ubisoft advices to reach 10 lvl here before applying on Intern/Junior GPP programmer*     
https://www.codingame.com/

Сode Wars (lots of task on different topics)   
https://www.codewars.com

Project Euler (mostly math tasks)    
*Vostok games (ex. GSC/STALKER team) advises to try this before applying to the position of GPP programmer*    
https://projecteuler.net/

   
>> TO-ADD: *University Olympiad Tasks*

----
## AI


---
## Mobile

---
## Math 

Math courses on Linear Algera, Calculus, Differential equations   
*Linear Algebr and 3D math is mandatory*   
https://www.khanacademy.org/math/

--- 
## Render


--- 
## AR/VR/MR

Virtual Reality Specialization ( University of London, Coursera)      
https://www.coursera.org/specializations/virtual-reality


--- 
## General game development

Game Design and Development Specialization   
https://www.coursera.org/specializations/game-development


Online Games: Literature, New Media, and Narrative   
https://www.coursera.org/learn/interactive-media-gaming

--- 
## Level Design 

Game Design: Art and Concepts Specialization (CALARTS, Coursera)    
https://www.coursera.org/specializations/game-design


--- 
## Game Design



- - -
## Sound Design
 /In Progress/

--- 
## 2D Art
 /In Progress/

 ---
## UI Development

Visual Elements of User Interface Design    
https://www.coursera.org/learn/visual-elements-user-interface-design

UX Design Fundamentals   
https://www.coursera.org/learn/ux-design-fundamentals

Unity UI Documentation  
https://docs.unity3d.com/Manual/UISystem.html

Unity UI Tutorials   
https://unity3d.com/learn/tutorials/s/user-interface-ui

UE4 UMG UI Documentation   
https://docs.unrealengine.com/en-us/Engine/UMG

---
## 3D Art
 /In Progress/



## Gamdev Practice



